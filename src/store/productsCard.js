import { createSlice } from "@reduxjs/toolkit";

const productsCardSlice = createSlice({
	name: "productsCard",
	initialState: {
		products: [],
		items: 0
	},
	reducers: {
		add: (state, action) => {
			state.products.push(action.payload);
		},
		remove: (state, action) => {
			for (var i = 0; i < state.products.length; i++) {
				if (state.products[i].id == action.payload) {
					state.products.splice(i, 1);
					return;
				}
			}
		},
		deleteState: (state, action) => {
			state.products = state.products.filter(function(i) {
				return i.id !== action.payload;
			});
		},
		numberItems: (state, action) => {
			state.items = Object.keys(action.payload).length;
		}
	}
});

export const {
	add,
	remove,
	deleteState,
	numberItems
} = productsCardSlice.actions;

export default productsCardSlice.reducer;
