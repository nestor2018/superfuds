import { configureStore, combineReducers } from "@reduxjs/toolkit";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

import productsCardSlice from "./productsCard";
import modalSlice from "./showModal";

const reducer = combineReducers({
	productsCard: productsCardSlice,
	modal: modalSlice
});

const persistConfig = {
	key: "root",
	storage: storage,
	//almacena las propiedades de state que queremos guardar
	whitelist: ["productsCard"]
	//almacena las propiedades del estate que no queremos guardar
	//blacklist: [],
};

const persistedReducer = persistReducer(persistConfig, reducer);

export const store = configureStore({
	reducer: persistedReducer
});

export const persistor = persistStore(store);
