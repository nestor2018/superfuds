import React from "react";

import "../styles/components/Sellos.scss";

const Sellos = ({ data }) => {
	return (
		<div className="Sellos">
			<img src={data.image} />
			<div className="Sellos-tooltip">
				<span className="Sellos-title">Producto</span>
				<br />
				<span>{data.name}</span>
			</div>
		</div>
	);
};

export default Sellos;
