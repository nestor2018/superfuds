import React from "react";
import { Navbar, Nav, NavDropdown } from "react-bootstrap";
import { FaSeedling, FaShoppingCart, FaChevronDown } from "react-icons/fa";
import { useDispatch } from "react-redux";

import "../styles/components/Header.scss";
import { changeShow } from "../store/showModal";

const Header = () => {
	let dispatch = useDispatch();
	return (
		<Navbar
			collapseOnSelect
			expand="lg"
			bg="green"
			className="p-3"
			variant="dark"
		>
			<Navbar.Brand>
				<FaSeedling className="mr-2" />
				superfüds
			</Navbar.Brand>
			<Navbar.Toggle aria-controls="responsive-navbar-nav" />
			<Navbar.Collapse id="responsive-navbar-nav">
				<Nav className="Header-body">
					<input
						className="Header-search"
						placeholder="Buscar marcas o productos"
					/>
					<button
						onClick={() => {
							dispatch(changeShow());
						}}
					>
						<FaShoppingCart className="" />
					</button>
					<div className="Header-container_image">
						<div className="Header-container_text mr-3">
							<p>Saiba Alimentos</p>
							<span>
								Mi perfil <FaChevronDown className="ml-2" />
							</span>
						</div>
						<img src="https://picsum.photos/200/300" className="Header-image" />
					</div>
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	);
};

export default Header;
