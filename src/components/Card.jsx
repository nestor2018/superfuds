import React from "react";
import { useDispatch } from "react-redux";

import "../styles/components/Card.scss";
import Sellos from "./Sellos";
import { add } from "../store/productsCard";

const Card = ({ product }) => {
	let dispatch = useDispatch();
	return (
		<div className="Card-container">
			<div className="Card">
				<div className="Card-container_image">
					<img src={product.thumbnail} />
					<div className="Card-sellos">
						{product.sellos.map(item => {
							return <Sellos key={item.name} data={item} />;
						})}
					</div>
				</div>
				<hr />
				<div className="Card-body_header">
					<span>{product.supplier}</span>
					<span>{product.net_content}</span>
				</div>
				<p className="Card-title">{product.title}</p>
				<p className="Card-price">
					<span className="Card-symbol">$</span>
					{product.price_real}
					<span className="Card-units">x{product.units_sf} unids</span>
				</p>
			</div>
			<button onClick={()=>{dispatch(add(product))}} className="Card-button">
				Agregar al carrito
			</button>
		</div>
	);
};

export default Card;
