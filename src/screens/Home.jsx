import React from "react";
import Slider from "react-slick";
import { FaChevronLeft, FaChevronRight } from "react-icons/fa";
import { useSelector } from "react-redux";
import { Modal } from "react-bootstrap";

import useInitialState from "../hooks/useInitialState.js";
import Card from "../components/Card";
import "../styles/components/Home.scss";
import apiConfig from "../config/api";
import Header from "../components/Header";
import ShoppingCart from "../components/ShoppingCart";

const API = apiConfig.domain;
const settings = {
	dots: false,
	arrows: true,
	infinite: true,
	speed: 500,
	slidesToShow: 5,
	slidesToScroll: 1,
	prevArrow: <FaChevronLeft />,
	nextArrow: <FaChevronRight />,
	responsive: [
		{
			breakpoint: 1300,
			settings: {
				slidesToShow: 4,
				slidesToScroll: 4,
				infinite: true,
				dots: false
			}
		},
		{
			breakpoint: 1024,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
				infinite: true,
				dots: false
			}
		},
		{
			breakpoint: 600,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2,
			}
		},
		{
			breakpoint: 480,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}
	]
};
const Home = () => {
	const show = useSelector(state => state.modal.show);
	const initialState = useInitialState(API);
	return (
		<>
			<Header />
			<div className="Home">
				<Slider {...settings}>
					{initialState.map(item => {
						return <Card key={item.id} product={item} />;
					})}
				</Slider>
				<Modal show={show}>
					<ShoppingCart />
				</Modal>
			</div>
		</>
	);
};

export default Home;
