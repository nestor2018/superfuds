const ProductMock = {
	id: 744,
	title: "Mezcla Para Pancakes Y Waffles Sabor A Brownie 300 g",
	supplier: "Why Not",
	tax: "0.19",
	price_real: "19751",
	image:
		"https://superfuds-file.s3.us-west-2.amazonaws.com/product/7709296944182_5e55873440167.png",
	thumbnail:
		"https://superfuds-file.s3.us-west-2.amazonaws.com/product/thumbnail/7709296944182_5e55873440167.png",
	description:
		"El chocolate es el placer culposo favorito por excelencia, incluso es el amor platónico de muchos. En la antigüedad los aztecas lo usaban como moneda y era más preciado que el oro. Hemos escogido al chocolate como el protagonista de nuestra mezcla para hacer pancakes, waffles y muffins, en una textura melcochuda, deliciosa y saludable. ",
	units_sf: 1,
	slug: "mezcla-para-pancakes-y-waffles-sabor-a-brownie-300-g",
	category: "Despensa",
	subcategory: "Mezclas",
	net_content: "300g",
	sellos: [
		{
			name: "vegano",
			image:
				"https://s3-sa-east-1.amazonaws.com/assets.superfuds.com/assets/ecom/v3/Group.png"
		},
		{
			name: "khoser",
			image:
				"https://s3-sa-east-1.amazonaws.com/assets.superfuds.com/assets/ecom/v3/Group1.png"
		},
		{
			name: "organico",
			image:
				"https://s3-sa-east-1.amazonaws.com/assets.superfuds.com/assets/ecom/v3/Group2.png"
		}
	]
};

export default ProductMock;
