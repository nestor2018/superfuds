import React from "react";
import { Provider } from "react-redux";
import { persistor } from "../store";
import { PersistGate } from "redux-persist/integration/react";
import { configureStore, combineReducers } from "@reduxjs/toolkit";
import storage from "redux-persist/lib/storage";
import { persistStore, persistReducer } from "redux-persist";

import productsCardSlice from "../store/productsCard";
import modalSlice from "../store/showModal";

const reducer = combineReducers({
	productsCard: productsCardSlice,
	modal: modalSlice
});

const persistConfig = {
	key: "root",
	storage: storage
};

const persistedReducer = persistReducer(persistConfig, reducer);

const store = configureStore({
	reducer: persistedReducer
});

const ProviderMock = props => (
	<Provider store={store}>
		<PersistGate loading={null} persistor={persistor}>
			{props.children}
		</PersistGate>
	</Provider>
);

export default ProviderMock;
