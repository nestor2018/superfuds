import productsCardSlice from "../../store/productsCard";
import ProductMock from "../../__mocks__/ProductMock";
import { createSlice } from "@reduxjs/toolkit";

describe("reducers", () => {
	test("retornar initialState", () => {
		expect(productsCardSlice({}, "")).toEqual({});
	});
	test("add reducer", () => {
		const initialState = {products: []}
		const {actions, reducer} = createSlice({
			name: "productsCard",
			reducers: {
				add: (state, action) => {
					state.products.push(action.payload);
				}
			}
		});
		const payload = ProductMock;
		const expected = {
			products: [ProductMock]
		};
		expect(reducer(initialState, actions.add(payload))).toEqual(expected);
	});
});
