import React from "react";
import { mount, shallow } from "enzyme";
import { create } from "react-test-renderer";
import ProviderMock from "../../__mocks__/ProviderMock";
import Card from "../../components/Card";

describe("Card", () => {
	test("render del componente header", () => {
		const card = shallow(
			<ProviderMock>
				<Card />
			</ProviderMock>
		);
		expect(card.length).toEqual(1);
	});
});
