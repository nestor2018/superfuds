import React from "react";
import { mount } from "enzyme";
import { create } from "react-test-renderer";

import Sellos from "../../components/Sellos";
import ProductMock from "../../__mocks__/ProductMock";

describe("Sellos", () => {
	test("render del Sellos", () => {
		const sellos = mount(<Sellos data={ProductMock} />);
		expect(sellos.length).toEqual(1);
	});
	test("render de texto", () => {
		const sellos = mount(<Sellos data={ProductMock} />);
		expect(sellos.find(".Sellos-title").text()).toEqual("Producto");
	});
	test("render de nombre del sello", () => {
		const sellos = mount(<Sellos data={ProductMock} />);
		expect(sellos.find('[span="ProductMock.name"]'));
	});
	test("render de la imagen del sello", () => {
		const sellos = mount(<Sellos data={ProductMock} />);
		expect(sellos.find('[img="ProductMock.image"]'));
	});
});

describe("Header SnapShot", () => {
	test("Comprobar la UI del componente Header", () => {
		const sellos = create(<Sellos data={ProductMock} />);
		expect(sellos.toJSON()).toMatchSnapshot();
	});
});
