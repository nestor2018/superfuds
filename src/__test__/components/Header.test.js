import React from "react";
import { mount, shallow } from "enzyme";
import { create } from "react-test-renderer";
import ProviderMock from "../../__mocks__/ProviderMock";
import Header from "../../components/Header";

describe("Header", () => {
	test("render del componente header", () => {
		const header = shallow(
			<ProviderMock>
				<Header />
			</ProviderMock>
		);
		expect(header.length).toEqual(1);
	});
	test("render de texto del header", () => {
		const header = shallow(
			<ProviderMock>
				<Header />
			</ProviderMock>
		);
		expect(header.find('[p="Saiba Alimentos"]'));
	});
});

describe("Header SnapShot", () => {
	test("Comprobar la UI del componente Header", () => {
		const header = create(
			<ProviderMock>
				<Header />
			</ProviderMock>
		);
		expect(header.toJSON()).toMatchSnapshot();
	});
});
